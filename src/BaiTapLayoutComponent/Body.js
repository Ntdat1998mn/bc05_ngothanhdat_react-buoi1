import React, { Component } from 'react';
import Banner from './Banner';
import Item from './Item';

class Body extends Component {
    render() {
        return (
            <section>
                <Banner/>
                <Item/>
            </section>
        );
    }
}

export default Body;